#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Stian Rødven Eide

import nltk
from nltk.corpus import senseval
from nltk.corpus import stopwords
import random

def extract_features(instance):
    '''A feature extration function for WSD. The features are the complete
    context, i.e. all words in the text with stopwords removed.'''
    features = {}
    context = [w for w in instance.context if type(w) == tuple]
    context = [w for w,p in context if w not in stopwords.words('english')]
    for word in context:
        features[word] = True
    return features
    
def yarowsky_bootstrapping(seeds, unlabeled, test_set, threshold=0.9, iterations=10):
    '''A bootstrapping algorithm for classification. 
    
    Given a seed of labeled examples and a set of unlabeled examples, 
    the algorithm trains a classifier first on the seeds only. Through 
    a number of iterations it includes classified unlabeled examples 
    in the training, if the classification confidence are above a 
    certain threshold. 
    
    This implementation uses a fixed set of iterations, rather than
    stopping on convergence.'''
    certain = seeds
    count = 0
    while count < iterations:
        if count == 0:
            classifier = nltk.NaiveBayesClassifier.train(seeds)
        count += 1
        certain = seeds
        for i in unlabeled:
            c = classifier.classify(i)
            prob = classifier.prob_classify(i).prob(c)
            if prob > threshold:
                certain.append((i,c))
        classifier = nltk.NaiveBayesClassifier.train(certain)
        accuracy = nltk.classify.accuracy(classifier, test_set)
        if count == 1:
            print "Accuracy after %s iteration: %.5f" % (count, accuracy)
        else:
            print "Accuracy after %s iterations: %.5f" % (count, accuracy)
            

if __name__ == '__main__':
    lines = senseval.instances('line.pos')

    cords = [l for l in lines if l.senses[0] == 'cord']           # 373 instances
    divisions = [l for l in lines if l.senses[0] == 'division']   # 374 instances
    #products = [l for l in lines if l.senses[0] == 'product']     # 2217 instances
    #texts = [l for l in lines if l.senses[0] == 'text']           # 404 instances
    #phones = [l for l in lines if l.senses[0] == 'phone']         # 429 instances
    #formations = [l for l in lines if l.senses[0] == 'formation'] # 349 instances

    mixed = cords + divisions # texts # phones # formations
    random.seed(13)
    random.shuffle(mixed)
    word = mixed[0].word
    senses = set([l.senses[0] for l in mixed])

    print 'Disambiguating word "%s" with %s senses' % (word, len(senses))
    print 'Senses used:',
    print ', '.join(senses)

    train_size = int(0.5 * len(mixed))
    train_instances = mixed[:train_size]
    test_instances = mixed[train_size:]

    print "Using %s instances for training and %s for testing" % (train_size, (len(mixed)-train_size))

    train_set = [ (extract_features(i), i.senses) for i in train_instances ]
    test_set = [ (extract_features(i), i.senses) for i in test_instances ]

    print "Training Naive Bayes classifier on complete training set..."
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    accuracy = nltk.classify.accuracy(classifier, test_set)
    print "Accuracy of Naive Bayes on test set: %.3f" % accuracy
    
    percentages = [5, 10, 20, 30, 40, 50]
    for percentage in percentages:
        print "Setting number of seeds to %s%% of training set" % percentage
        seeds_size = int((percentage / 100.0) * train_size)
        seeds = train_set[:seeds_size]
        unlabeled_train = [features for (features, label) in train_set[seeds_size:]]

        print "Number of seeds: %s" % len(seeds)
        print "Size of unlabeled training set: %s" % len(unlabeled_train)

        print "Training Naive Bayes classifier on seeds only..."
        classifier = nltk.NaiveBayesClassifier.train(seeds)
        accuracy = nltk.classify.accuracy(classifier, test_set)
        print "Accuracy of Naive Bayes on test set: %.3f" % accuracy

        thresholds = [0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.98, 0.99]
        for threshold in thresholds:
            print "Starting Yarowsky Bootstrapping with threshold %s" % threshold
            yarowsky_bootstrapping(seeds, unlabeled_train, test_set, threshold)
        
